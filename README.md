# FindHotel Code Challenge App

### Run tests:
```console
npm run test
```

### Run in dev mode:
```console
npm run dev
```

#### Run in production mode:
```
npm run build && npm run prod
```


const presets = [
  '@babel/preset-typescript',
  ['@babel/preset-react', { runtime: 'automatic', importSource: '@emotion/react' }],
  [
    '@babel/preset-env',
    {
      useBuiltIns: 'usage',
      corejs: 3,
    },
  ],
];

const plugins = [
  ['@babel/plugin-transform-runtime', { corejs: 3 }],
  '@emotion/babel-plugin',
  [
    'module-resolver',
    {
      alias: {
        '~store': './src/store',
        '~testing': './src/testing',
        '~features': './src/features',
      },
    },
  ],
];

module.exports = { presets, plugins };

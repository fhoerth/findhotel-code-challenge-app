const babelConfig = require('../babel.config');

module.exports = {
  preset: 'ts-jest',
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  globals: {
    'ts-jest': { babelConfig },
  },
  rootDir: '../',
  setupFilesAfterEnv: ['<rootDir>/jest/setup.js'],
  transform: {
    '^.+\\.svg$': '<rootDir>/jest/svgTransform.js',
  },
};

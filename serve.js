const fs = require('fs');
const path = require('path');
const http = require('http');
const process = require('process');
const finalhandler = require('finalhandler');
const serveStatic = require('serve-static');

const env = require('./env');
const config = require('./config');

const serve = serveStatic(config.directories.output);

fs.readFile(path.join(config.directories.output, 'index.html'), (readError, buffer) => {
  if (readError) {
    console.error(readError);

    process.exit(1);
  } else {
    function indexHtmlHandler(req, res, err) {
      return () => {
        const done = finalhandler(req, res);

        if (err) {
          done(err);
        } else {
          res.setHeader('Content-Type', 'text/html');
          res.end(buffer);
        }
      };
    }

    const server = http.createServer(function handleRequest(req, res) {
      serve(req, res, indexHtmlHandler(req, res));
    });

    server.listen(env.port, () => {
      console.log(`Application available at http://localhost:${env.port}`);
    });
  }
});

const process = require('process');

const isProduction = !!(process.env.NODE_ENV && process.env.NODE_ENV === 'production');
const isDevelopment = !isProduction;
const env = isProduction ? 'production' : 'development';

module.exports = {
  env,
  isProduction,
  isDevelopment,
  port: process.env.PORT || 5000,
};

import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore as createReduxStore, Store as ReduxStore } from 'redux';

import Store from './Store';

import reducer from './reducer';

function createStore(
  initialState?: Store.RootState,
): ReduxStore<Store.RootState, Store.RootAction> {
  const store = createReduxStore(reducer, initialState, composeWithDevTools());

  return store;
}

export default createStore;

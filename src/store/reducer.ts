import { Rooms, roomsReducer } from '~features/rooms';
import Store from './Store';

function reducer(state: Store.RootState, action: Store.RootAction): Store.RootState {
  switch (action.type) {
    case Store.ActionType.RESET_ROOMS: {
      return {
        rooms: {
          ...state.rooms,
          nextState: state.rooms.currentState,
        },
      };
    }
    case Store.ActionType.COMMIT_ROOMS: {
      return {
        rooms: {
          ...state.rooms,
          currentState: state.rooms.nextState,
        },
      };
    }
    default: {
      return {
        rooms: {
          ...state.rooms,
          nextState: roomsReducer(state.rooms.nextState, action),
        },
      };
    }
  }
}

function rootReducer(
  state: Store.RootState | undefined,
  action: Store.RootAction,
): Store.RootState {
  if (!state) {
    const currentState = roomsReducer(state, action as Rooms.Action);

    return {
      rooms: { currentState, nextState: currentState },
    };
  }

  return reducer(state, action);
}

export default rootReducer;

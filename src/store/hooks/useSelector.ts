import { useSelector as useReduxSelector, TypedUseSelectorHook } from 'react-redux';

import Store from '../Store';

const useSelector: TypedUseSelectorHook<Store.RootState> = useReduxSelector;

export default useSelector;

import { Dispatch } from 'redux';
import { useDispatch as useReduxDispatch } from 'react-redux';

import Store from '../Store';

function useDispatch<T extends Store.RootAction>(): Dispatch<T> {
  return useReduxDispatch<Dispatch<T>>();
}

export default useDispatch;

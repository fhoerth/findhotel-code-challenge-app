import { Rooms } from '~features/rooms';

module Store {
  export enum ActionType {
    RESET_ROOMS = '@@store/RESET_ROOMS',
    COMMIT_ROOMS = '@@store/COMMIT_ROOMS',
  }

  export type ResetRoomsAction = {
    type: ActionType.RESET_ROOMS;
  };

  export type CommitRoomsAction = {
    type: ActionType.COMMIT_ROOMS;
  };

  export type RootState = {
    rooms: {
      currentState: Rooms.State;
      nextState: Rooms.State;
    };
  };

  export type RootAction = Rooms.Action | ResetRoomsAction | CommitRoomsAction;
}

export default Store;

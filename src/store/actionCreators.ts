import Store from './Store';

export const resetRooms = (): Store.ResetRoomsAction => ({
  type: Store.ActionType.RESET_ROOMS,
});

export const commitRooms = (): Store.CommitRoomsAction => ({
  type: Store.ActionType.COMMIT_ROOMS,
});

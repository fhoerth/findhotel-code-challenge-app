import React from 'react';
import { fireEvent, render } from '~testing/utils';

import GuestsAndRoomsModal from '../components/GuestsAndRoomsModal';

describe('Search', () => {
  test('"Add Room" adds a room', async () => {
    const { getByText, getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    expect(getAllByTestId('room')).toHaveLength(1);

    const element = getByText('Add Room');
    fireEvent.click(element);

    expect(getAllByTestId('room')).toHaveLength(2);
  });

  test('No more than 8 rooms can be added', () => {
    const { getByText, getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const element = getByText('Add Room');

    Array.from(Array(10)).forEach(() => {
      fireEvent.click(element);
    });

    expect(getAllByTestId('room')).toHaveLength(8);
  });

  test(`first room can't be removed`, () => {
    const { getByText, getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const removeRoomElement = getByText('Remove room');

    fireEvent.click(removeRoomElement);

    expect(getAllByTestId('room')).toHaveLength(1);
  });

  test('rooms can be removed', () => {
    const { getByText, getAllByText, queryByText } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const addRoomElement = getByText('Add Room');

    Array.from(Array(10)).forEach(() => {
      fireEvent.click(addRoomElement);
    });

    const removeRoomElements = getAllByText('Remove room');

    const [, ...rooms] = removeRoomElements;

    rooms.forEach((element, idx) => {
      fireEvent.click(element);

      expect(queryByText(`Room ${8 - idx}`)).toBe(null);
    });
  });

  test('adult guests can be added', () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [incrementAdultGuestsButton] = getAllByTestId('guests-counter-increment');

    fireEvent.click(incrementAdultGuestsButton);

    const [adultGuests] = getAllByTestId('guests-counter-value');

    expect(adultGuests.textContent).toBe('3');
  });

  test('adult guests can be removed', () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [decrementAdultGuestsButton] = getAllByTestId('guests-counter-decrement');

    fireEvent.click(decrementAdultGuestsButton);

    const [adultGuests] = getAllByTestId('guests-counter-value');

    expect(adultGuests.textContent).toBe('1');
  });

  test('rooms require at least one adult', () => {
    const handleSearch = jest.fn();
    const { getByTestId, getByText, getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={handleSearch} />,
    );

    const addRoomElement = getByText('Add Room');

    fireEvent.click(addRoomElement);

    const [, , incrementAdultGuestsButton] = getAllByTestId('guests-counter-increment');

    fireEvent.click(getByText(/2 guests/));

    expect(handleSearch).not.toHaveBeenCalled();

    fireEvent.click(incrementAdultGuestsButton);
    fireEvent.click(getByTestId('modal-search-button'));

    expect(handleSearch).toHaveBeenCalled();
  });

  test(`rooms don't allow more than 5 adults`, () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [incrementAdultGuestsButton] = getAllByTestId('guests-counter-increment');

    Array.from(Array(10)).forEach(() => {
      fireEvent.click(incrementAdultGuestsButton);
    });

    const [adultGuests] = getAllByTestId('guests-counter-value');

    expect(adultGuests.textContent).toBe('5');
  });

  test('child guests can be added and removed', () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [, incrementChildGuestsButton] = getAllByTestId('guests-counter-increment');
    const [, decrementChildGuestsButton] = getAllByTestId('guests-counter-decrement');

    fireEvent.click(incrementChildGuestsButton);
    fireEvent.click(decrementChildGuestsButton);

    const [, children] = getAllByTestId('guests-counter-value');

    expect(children.textContent).toBe('0');
  });

  test(`rooms don't allow more than 3 child guests`, () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [, incrementChildGuestsButton] = getAllByTestId('guests-counter-increment');

    fireEvent.click(incrementChildGuestsButton);
    fireEvent.click(incrementChildGuestsButton);
    fireEvent.click(incrementChildGuestsButton);

    const [, children] = getAllByTestId('guests-counter-value');

    expect(children.textContent).toBe('3');
  });

  test(`rooms don't allow more than 5 guests`, () => {
    const { getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={jest.fn()} />,
    );

    const [incrementAdultGuestsButton, incrementChildGuestsButton] = getAllByTestId(
      'guests-counter-increment',
    );

    fireEvent.click(incrementAdultGuestsButton);

    fireEvent.click(incrementChildGuestsButton);
    fireEvent.click(incrementChildGuestsButton);
    fireEvent.click(incrementChildGuestsButton);

    const [adults, children] = getAllByTestId('guests-counter-value');

    expect(adults.textContent).toBe('3');
    expect(children.textContent).toBe('2');
  });

  test('search button shows total rooms and guests', () => {
    const handleSearch = jest.fn();
    const { getByText, getAllByTestId } = render(
      <GuestsAndRoomsModal visible onClose={jest.fn()} onSearch={handleSearch} />,
    );

    const addRoomElement = getByText('Add Room');

    fireEvent.click(addRoomElement);

    const [incrementRoom1AdultGuests, , incrementRoom2AdultGuests] = getAllByTestId(
      'guests-counter-increment',
    );

    Array.from(Array(5)).forEach(() => {
      fireEvent.click(incrementRoom1AdultGuests);
      fireEvent.click(incrementRoom2AdultGuests);
    });

    getByText(/2 rooms/);
    getByText(/10 guests/);
  });
});

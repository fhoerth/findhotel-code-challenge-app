import React from 'react';
import { fireEvent, render } from '~testing/utils';

import Routes from '../components/Routes';

describe('App', () => {
  test('serializes rooms', () => {
    const { getByText, getByTestId, navigator } = render(<Routes />);

    const button = getByTestId('guests-button');

    fireEvent.click(button);
    fireEvent.click(getByText(/2 guests/));

    expect(navigator.location.search).toBe('?rooms=2');
  });

  test('parses rooms', () => {
    const { getByText, getByTestId } = render(<Routes />, '?rooms=5');

    const button = getByTestId('guests-button');

    fireEvent.click(button);

    getByText(/5 guests/);
  });
});

const theme = {
  borderRadius: '4px',
  searchBoxPadding: '0.7em',
  modalWidth: '22.5rem',
  modalInputPadding: '0.375em',
  global: {
    fontSize: '16px',
  },
  content: {
    maxWidth: '22.5rem',
  },
  colors: {
    blue: '#0171f3',
    lightBlue: '#dbfaff',
    lighterOutline: '#ecf0f9',
    outline: '#d4dee9',
    darkerOutline: '#c4cdd8',
    lightGray: '#f7faff',
    gray: '#999',
    darkGray: '#3d434c',
    cyan: '#3fc8d9',
    lighterSeparator: '#dfe3e8',
    white: '#fefefe',
    black: '#030303',
    warning: '#d24345',
  },
  button: {
    fontSize: '1em',
    lineHeight: '1.2em',
    padding: '0.7em',
  },
  input: {
    fontSize: '1em',
    lineHeight: '1.2em',
    padding: '0.7em',
  },
};

type Theme = typeof theme;

export { Theme };
export default theme;

const config = {
  childrenMaxAge: 8,
  queryParams: {
    rooms: 'rooms',
  },
  rooms: {
    defaults: {
      adults: 1,
    },
    constraints: {
      maxRooms: 8,
      minAdultGuests: 1,
      maxChildGuests: 3,
      maxGuests: 5,
    },
  },
  locationParser: {
    delimiters: {
      room: '|',
      guests: ':',
      child: ',',
    },
  },
};

export default config;

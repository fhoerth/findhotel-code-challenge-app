import React from 'react';
import { Router } from 'react-router-dom';
import { createMemoryHistory, Location, MemoryHistory } from 'history';
import { Provider as ReduxProvider } from 'react-redux';
import { Store as ReduxStore } from 'redux';
import { ThemeProvider } from '@emotion/react';

import { render } from '@testing-library/react';

import { createStore, Store } from '~store';

import theme from '../theme';

type CustomRenderResult = ReturnType<typeof render> & {
  store: ReduxStore<Store.RootState, Store.RootAction>;
  initialState: Store.RootState;
  navigator: MemoryHistory;
};

type ProviderProps = {
  children: React.ReactNode;
};

function createProvider(
  navigator: MemoryHistory,
  store: ReduxStore<Store.RootState, Store.RootAction>,
  location: Location,
): React.FC<ProviderProps> {
  const Provider: React.FC<ProviderProps> = ({ children }: ProviderProps) => (
    <ThemeProvider theme={theme}>
      <ReduxProvider store={store}>
        <Router location={location} navigator={navigator}>
          {children}
        </Router>
      </ReduxProvider>
    </ThemeProvider>
  );

  return Provider;
}

function customRender(
  ui: React.ReactElement,
  initialState?: Store.RootState,
  search?: string,
  pathname?: string,
): CustomRenderResult;
function customRender(
  ui: React.ReactElement,
  search?: string,
  pathname?: string,
): CustomRenderResult;
function customRender(
  ui: React.ReactElement,
  initialStateOrSearch?: Store.RootState | string,
  pathnameOrSearchParam?: string,
  pathnameParam?: string,
): CustomRenderResult {
  const search =
    typeof initialStateOrSearch === 'string' ? initialStateOrSearch : pathnameOrSearchParam || '';
  const pathname =
    (initialStateOrSearch === 'string' ? pathnameOrSearchParam : pathnameParam) || '/';
  const location = {
    pathname,
    search,
    key: 'default',
    hash: '',
    state: {},
  };

  const store = createStore(
    typeof initialStateOrSearch !== 'string' ? initialStateOrSearch : undefined,
  );
  const storeInitialState = store.getState();
  const navigator = createMemoryHistory();
  const Provider = createProvider(navigator, store, location);
  const result = render(ui, { wrapper: Provider as React.FC });

  return {
    ...result,
    store,
    navigator,
    initialState: storeInitialState,
  };
}

export * from '@testing-library/react';

export { customRender as render };

import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, Global } from '@emotion/react';

import theme from '../../theme';

import Routes from '../Routes';

import global from './css';

const App: React.FC = () => (
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <Global styles={global} />
      <Routes />
    </BrowserRouter>
  </ThemeProvider>
);

export default App;

import { css } from '@emotion/react';

import theme from '../../theme';

const global = css`
  html,
  body {
    margin: 0;
    padding: 0;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
    font-size: ${theme.global.fontSize};
  }
`;

export default global;

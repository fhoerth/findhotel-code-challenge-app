import React from 'react';
import { Route, Routes as ReactRoutes } from 'react-router-dom';

import Store from './Store';
import Home from './Home';

const Routes: React.FC = () => (
  <ReactRoutes>
    <Route
      path="/"
      element={
        <Store parseRoomsFromLocation>
          <Home />
        </Store>
      }
    />
  </ReactRoutes>
);

export default Routes;

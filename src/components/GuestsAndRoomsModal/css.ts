import { css, keyframes } from '@emotion/react';

import theme from '../../theme';

const animation = '300ms cubic-bezier(.17,.67,.72,.68)';
const opacity = keyframes`
  from { opacity: 0 }
  to { opacity: 1 }
`;

export const closeIcon = css`
  cursor: pointer;
  fill: ${theme.colors.gray};
  float: left;
`;

export const overlay = css`
  animation: ${opacity} ${animation};
  background: rgba(42, 51, 61, 0.6);
  height: 100%;
  left: 0px;
  opacity: 1;
  position: fixed;
  top: 0px;
  transition: opacity ${animation};
  width: 100%;
  z-index: 20;
`;

export default closeIcon;

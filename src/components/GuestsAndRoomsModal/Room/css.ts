import { css } from '@emotion/react';

import theme from '../../../theme';

const closeIcon = css`
  cursor: pointer;
  fill: ${theme.colors.warning};
  margin-left: 0.4em;
`;

export default closeIcon;

import React from 'react';

import useDispatch from '~store/hooks/useDispatch';
import { roomsActionCreators, Rooms } from '~features/rooms';

import CloseIcon from '../../../icons/Close';
import AgeSelectInput from '../../inputs/AgeSelect';

import { Row, Label, Input } from './styled';
import closeIcon from './css';

type ChildAgeProps = {
  id: Rooms.RoomId;
  child: Rooms.Child;
  childNumber: number;
};
const ChildAge: React.FC<ChildAgeProps> = ({ id, child, childNumber }: ChildAgeProps) => {
  const dispatch = useDispatch();

  const handleAgeChange = (age: number | undefined) => {
    dispatch(roomsActionCreators.updateChildGuest({ id, child, data: { age } }));
  };

  const handleRemove = () => {
    dispatch(roomsActionCreators.removeChildGuest({ id, child }));
  };

  return (
    <Row expanded>
      <Label expanded>Child {childNumber} age</Label>
      <Input>
        <AgeSelectInput defaultValue={child.age} onAgeChange={handleAgeChange} />
        <CloseIcon css={closeIcon} onClick={handleRemove} />
      </Input>
    </Row>
  );
};

export default ChildAge;

import styled from '@emotion/styled';

type Expanded = { expanded?: boolean };
type Disabled = { disabled?: boolean };

export const Wrapper = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.lighterOutline};
  margin-bottom: 1em;
  padding-bottom: 1em;
`;

export const Row = styled.div<Expanded>`
  align-items: center;
  border-left: ${({ theme, expanded }) =>
    expanded ? `1px solid ${theme.colors.lighterOutline}` : 'none'};
  display: flex;
  justify-content: space-between;
  margin-left: ${({ expanded }) => (expanded ? '0.5em' : 0)};
  padding-bottom: ${({ expanded }) => (expanded ? '0.5em' : '0.5em')};
  padding-top: ${({ expanded }) => (expanded ? '0.5em' : '0.5em')};
`;

export const Title = styled.h5`
  font-size: 1em;
  margin: 0 0 0.75em;
  padding: 0;
  user-select: none;
`;

export const RoomRemove = styled.span<Disabled>`
  color: ${({ theme }) => theme.colors.warning};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
  float: right;
  font-size: 0.9em;
  opacity: ${({ disabled }) => (disabled ? '.5' : '1')};
`;

export const Label = styled.div<Expanded>`
  color: ${({ theme }) => theme.colors.darkGray};
  font-size: 0.9em;
  font-weight: 700;
  margin-right: auto;
  padding-left: ${({ expanded }) => (expanded ? '0.5em' : 0)};
`;

export const Input = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  min-width: 6em;
`;

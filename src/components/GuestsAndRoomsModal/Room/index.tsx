import React, { useCallback } from 'react';

import useDispatch from '~store/hooks/useDispatch';
import Constraints from '~features/rooms/Constraints';
import { roomsActionCreators, Rooms } from '~features/rooms';

import GuestsCounterInput from '../../inputs/GuestsCounter';

import ChildAge from './childAge';
import { Wrapper, Row, Title, RoomRemove, Label, Input } from './styled';

type RoomProps = {
  room: Rooms.Room;
  roomNumber: number;
  enableRemove: boolean;
};

const Room: React.FC<RoomProps> = ({ room, roomNumber, enableRemove }: RoomProps) => {
  const dispatch = useDispatch();
  const { id, guests } = room;

  const { adults, children } = guests;

  const handleRoomRemove = useCallback(() => {
    dispatch(roomsActionCreators.removeRoom({ id }));
  }, []);

  const handleAdultGuestsDecrement = useCallback(() => {
    dispatch(roomsActionCreators.removeAdultGuest({ id }));
  }, []);

  const handleAdultGuestsIncrement = useCallback(() => {
    dispatch(roomsActionCreators.addAdultGuest({ id }));
  }, []);

  const handleChildGuestsDecrement = useCallback(() => {
    dispatch(roomsActionCreators.removeLastChildGuest({ id }));
  }, []);

  const handleChildGuestsIncrement = useCallback(() => {
    dispatch(roomsActionCreators.addChildGuest({ id }));
  }, []);

  return (
    <Wrapper data-testid="room">
      <Title>
        Room {roomNumber}
        <RoomRemove onClick={handleRoomRemove} disabled={!enableRemove}>
          Remove room
        </RoomRemove>
      </Title>
      <Row>
        <Label>Adults</Label>
        <Input>
          <GuestsCounterInput
            value={adults}
            disableDecrement={!Constraints.canAdultGuestBeRemoved(room)}
            disableIncrement={!Constraints.canAdultGuestBeAdded(room)}
            onDecrement={handleAdultGuestsDecrement}
            onIncrement={handleAdultGuestsIncrement}
          />
        </Input>
      </Row>
      <Row>
        <Label>Children</Label>
        <Input>
          <GuestsCounterInput
            value={children.length}
            disableIncrement={!Constraints.canChildGuestBeAdded(room)}
            onDecrement={handleChildGuestsDecrement}
            onIncrement={handleChildGuestsIncrement}
          />
        </Input>
      </Row>
      {children.map((child, idx) => (
        <ChildAge key={child.key} id={id} child={child} childNumber={idx + 1} />
      ))}
    </Wrapper>
  );
};

export default Room;

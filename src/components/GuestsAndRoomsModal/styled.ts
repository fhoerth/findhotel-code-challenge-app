import styled from '@emotion/styled';

export const ModalWrapper = styled.div`
  background: ${({ theme }) => theme.colors.white};
  border-radius: ${({ theme }) => theme.borderRadius};
  box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  display: flex;
  flex-direction: column;
  margin: 0 auto 15%;
  max-height: 84%;
  overflow: hidden;
  position: relative;
  top: 8%;
  width: ${({ theme }) => theme.modalWidth};
  z-index: 3;
`;

export const ModalHeader = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.lighterOutline};
`;

export const Title = styled.h3`
  color: ${({ theme }) => theme.colors.darkGray};
  font-size: 1.2em;
  font-weight: 400;
  margin: 0;
  padding: 1em;
  text-align: center;
  user-select: none;
`;

export const ModalScroll = styled.div`
  flex-grow: 1;
  overflow-y: auto;
`;

export const ModalContent = styled.div`
  min-height: 20em;
  padding: 1em;
`;

export const ModalFooter = styled.div`
  border-top: 1px solid ${({ theme }) => theme.colors.lighterOutline};
  padding: 1em;
`;

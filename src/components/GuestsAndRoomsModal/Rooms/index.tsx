import React, { useCallback } from 'react';

import useDispatch from '~store/hooks/useDispatch';
import Constraints from '~features/rooms/Constraints';
import { roomsActionCreators } from '~features/rooms';
import { createRoomId } from '~features/rooms/utils';
import { useRoomsSelector, useRoomsState } from '~features/rooms/selectors';

import AddRoomButton from '../../buttons/AddRoom';

import Room from '../Room';

const RoomsComponent: React.FC = () => {
  const dispatch = useDispatch();
  const rooms = useRoomsSelector({ from: 'nextState' });
  const state = useRoomsState({ from: 'nextState' });

  const handleRoomAdd = useCallback(() => {
    const id = createRoomId(state);

    dispatch(roomsActionCreators.addRoom({ id }));
  }, [state]);

  return (
    <>
      {rooms.map((room, idx) => (
        <Room
          enableRemove={Constraints.canRoomBeRemoved(rooms)}
          key={room.id}
          room={room}
          roomNumber={idx + 1}
        />
      ))}
      <AddRoomButton disabled={!Constraints.canRoomBeAdded(state)} onRoomAdd={handleRoomAdd} />
    </>
  );
};

export default RoomsComponent;

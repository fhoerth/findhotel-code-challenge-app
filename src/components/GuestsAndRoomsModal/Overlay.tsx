import React, { useCallback, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

const elementId = 'modal';
let modalElement = document.getElementById(elementId);

type OverlayProps = {
  children: React.ReactNode;
  className?: string;
};

const Overlay: React.FC<OverlayProps> = ({ children, className = '' }: OverlayProps) => {
  if (!modalElement) {
    modalElement = document.createElement('div');
    modalElement.setAttribute('id', elementId);

    document.body.appendChild(modalElement);
  }

  const element = useRef(document.createElement('div'));
  const { current: divElement } = element;

  divElement.className = className;

  const hideOverlay = useCallback(() => {
    divElement.style.opacity = '0';
  }, []);

  useEffect(() => {
    if (modalElement) {
      modalElement.appendChild(divElement);
    }

    return function cleanUp() {
      if (modalElement) {
        divElement.addEventListener('transitionend', () => {
          window.requestAnimationFrame(() => {
            modalElement!.removeChild(divElement);
          });
        });

        hideOverlay();
      }
    };
  }, []);

  return createPortal(children, divElement);
};

Overlay.defaultProps = {
  className: undefined,
};

export default Overlay;

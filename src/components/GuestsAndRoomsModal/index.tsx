import React from 'react';

import Constraints from '~features/rooms/Constraints';
import { useRoomsState, useRoomsSelector, useGuestsSelector } from '~features/rooms/selectors';

import { CloseIcon } from '../../icons';

import ModalSearchButton from '../buttons/ModalSearch';

import { closeIcon, overlay } from './css';
import { ModalWrapper, ModalHeader, Title, ModalScroll, ModalContent, ModalFooter } from './styled';

import Overlay from './Overlay';
import Rooms from './Rooms';

type GuestsAndRoomsModalProps = {
  visible: boolean;
  onClose: () => void;
  onSearch: () => void;
};

const GuestsAndRoomsModal: React.FC<GuestsAndRoomsModalProps> = ({
  visible,
  onClose,
  onSearch,
}: GuestsAndRoomsModalProps) => {
  const state = useRoomsState({ from: 'nextState' });
  const rooms = useRoomsSelector({ from: 'nextState' });
  const guests = useGuestsSelector({ from: 'nextState' });

  if (!visible) {
    return null;
  }

  return (
    <Overlay css={overlay}>
      <ModalWrapper>
        <ModalHeader>
          <Title>
            <CloseIcon css={closeIcon} onClick={onClose} />
            Who is staying?
          </Title>
        </ModalHeader>
        <ModalScroll>
          <ModalContent>
            <Rooms />
          </ModalContent>
        </ModalScroll>
        <ModalFooter>
          <ModalSearchButton
            disabled={!Constraints.canSearchBePerformed(state)}
            rooms={rooms.length}
            guests={guests}
            onSearch={onSearch}
          />
        </ModalFooter>
      </ModalWrapper>
    </Overlay>
  );
};

export default GuestsAndRoomsModal;

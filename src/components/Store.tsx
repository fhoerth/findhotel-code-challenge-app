import React from 'react';
import { Provider } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { actionCreators, createStore } from '~store';
import { QueryStringParser } from '~features/queryString';
import { roomsActionCreators, roomsParser } from '~features/rooms';

type StoreProps = {
  children: React.ReactNode;
  parseRoomsFromLocation?: boolean;
};

const Store: React.FC<StoreProps> = ({ parseRoomsFromLocation, children }: StoreProps) => {
  const store = createStore();

  if (parseRoomsFromLocation) {
    const location = useLocation();
    const stringifiedState = QueryStringParser.parseRooms(location);

    if (stringifiedState) {
      const parsedState = roomsParser(stringifiedState);

      store.dispatch(roomsActionCreators.hydrate(parsedState));
      store.dispatch(actionCreators.commitRooms());
    }
  }

  return <Provider store={store}>{children}</Provider>;
};

Store.defaultProps = {
  parseRoomsFromLocation: false,
};

export default Store;

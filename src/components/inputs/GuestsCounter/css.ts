import { css } from '@emotion/react';

import theme from '../../../theme';

export const wrapper = css`
  align-items: center;
  color: ${theme.colors.darkGray};
  display: flex;
  flex-direction: row;
  font-weight: 700;
  justify-content: space-between;
  line-height: ${theme.input.fontSize};
  width: 100%;
`;

export const button = css`
  padding: ${theme.modalInputPadding};
  width: auto;
`;

export const icon = css`
  fill: ${theme.colors.blue};
  height: 1em;
  width: 1em;
`;

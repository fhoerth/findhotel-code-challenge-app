import React from 'react';

import { PlusIcon, MinusIcon } from '../../../icons';

import Button from '../../Button';

import { wrapper, button, icon } from './css';

type GuestsCounterProps = {
  value: number;
  disableIncrement: boolean;
  onDecrement: () => void;
  onIncrement: () => void;
  disableDecrement?: boolean;
};

const GuestsCounter: React.FC<GuestsCounterProps> = ({
  value,
  disableDecrement,
  disableIncrement,
  onIncrement,
  onDecrement,
}: GuestsCounterProps) => {
  const handleDecrement = () => {
    if (value > 0) {
      onDecrement();
    }
  };

  return (
    <div css={wrapper}>
      <Button
        data-testid="guests-counter-decrement"
        outlined
        disabled={disableDecrement}
        css={button}
        onClick={handleDecrement}>
        <MinusIcon css={icon} />
      </Button>
      <span data-testid="guests-counter-value">{value}</span>
      <Button
        data-testid="guests-counter-increment"
        outlined
        disabled={disableIncrement}
        css={button}
        onClick={onIncrement}>
        <PlusIcon css={icon} />
      </Button>
    </div>
  );
};

GuestsCounter.defaultProps = {
  disableDecrement: undefined,
};

export default GuestsCounter;

import { css } from '@emotion/react';

import theme from '../../../theme';

export const wrapper = css`
  display: flex;
  position: relative;
  width: 100%;
`;

export const locationIcon = css`
  fill: ${theme.colors.blue};
  height: 1.2em;
  left: 0.5em;
  position: absolute;
  top: 0.7em;
  width: 1.2em;
`;

export const nearMeIcon = css`
  fill: ${theme.colors.cyan};
  height: 1.2em;
  position: absolute;
  right: 0.5em;
  top: 0.7em;
  width: 1.2em;
`;

export const input = css`
  background: ${theme.colors.white};
  border: 1px solid ${theme.colors.darkerOutline};
  border-radius: ${theme.borderRadius};
  flex: 1;
  font-size: ${theme.input.fontSize};
  font-weight: 700;
  line-height: ${theme.input.lineHeight};
  outline: none;
  padding: ${theme.input.padding} 2em;

  &::placeholder {
    color: ${theme.colors.gray};
  }
`;

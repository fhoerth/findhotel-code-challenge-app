import React from 'react';

import { LocationIcon, NearMeIcon } from '../../../icons';

import { wrapper, locationIcon, nearMeIcon, input } from './css';

const Location: React.FC = () => (
  <div css={wrapper}>
    <LocationIcon css={locationIcon} />
    <NearMeIcon css={nearMeIcon} />
    <input css={input} type="text" placeholder="Type city, place, or hotel name" />
  </div>
);

export default Location;

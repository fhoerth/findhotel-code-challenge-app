import styled from '@emotion/styled';

export const Wrapper = styled.div`
  &:after {
    color: ${({ theme }) => theme.colors.blue};
    content: '⌵';
    float: right;
    font-weight: 700;
    line-height: 1.4em;
    margin-right: 0.3em;
    pointer-events: none;
    position: absolute;
    right: 0;
  }
  display: flex;
  position: relative;
  width: 100%;
`;

export const Select = styled.select`
  appearance: none;
  background: transparent;
  border: 1px solid ${({ theme }) => theme.colors.darkerOutline};
  border-radius: ${({ theme }) => theme.borderRadius};
  font-size: ${({ theme }) => theme.input.fontSize};
  line-height: ${({ theme }) => theme.input.fontSize};
  max-height: ${({ theme }) => theme.input.fontSize + parseFloat(theme.modalInputPadding) * 2};
  max-height: ${1.2 + 0.375 * 2}em;
  outline-color: ${({ theme }) => theme.colors.blue};
  padding: ${({ theme }) => theme.modalInputPadding};
  width: 100%;
`;

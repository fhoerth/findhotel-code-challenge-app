import config from '../../../config';

const range = Array.from(Array(config.childrenMaxAge + 1).keys());

export default range;

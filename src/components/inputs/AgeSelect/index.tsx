import React from 'react';
import options from './options';

import { Wrapper, Select } from './styled';

type AgeSelectProps = {
  onAgeChange: (value: number | undefined) => void;
  defaultValue?: number;
};

const AgeSelect: React.FC<AgeSelectProps> = ({ defaultValue, onAgeChange }: AgeSelectProps) => {
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const { target } = event;
    const parsedValue = parseInt(target.value, 10);

    if (Number.isInteger(parsedValue)) {
      onAgeChange(parsedValue);
    }
  };

  const selectDefaultValue = typeof defaultValue === 'undefined' ? 'Age' : defaultValue;

  return (
    <Wrapper>
      <Select defaultValue={selectDefaultValue} onChange={handleChange}>
        <option disabled value="Age">
          Age
        </option>
        {options.map((option) => (
          <option key={option} label={option.toString()} value={option} />
        ))}
      </Select>
    </Wrapper>
  );
};

AgeSelect.defaultProps = {
  defaultValue: undefined,
};

export default AgeSelect;

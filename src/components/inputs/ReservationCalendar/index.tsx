import React from 'react';

import { CalendarIcon } from '../../../icons';

import { wrapper, icon, checkIn, checkOut } from './css';

type ReservationCalendarProps = {
  className?: string;
};

const ReservationCalendar: React.FC<ReservationCalendarProps> = ({
  className,
}: ReservationCalendarProps) => (
  <div css={wrapper} className={className}>
    <span css={checkIn}>
      <CalendarIcon css={icon} />
      Check-in
    </span>
    <span css={checkOut}>Check-out</span>
  </div>
);

ReservationCalendar.defaultProps = {
  className: undefined,
};

export default ReservationCalendar;

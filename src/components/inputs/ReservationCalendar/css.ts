import { css } from '@emotion/react';

import theme from '../../../theme';

export const wrapper = css`
  align-items: center;
  background: ${theme.colors.white};
  border: 1px solid ${theme.colors.darkerOutline};
  border-radius: ${theme.borderRadius};
  display: flex;
  justify-content: center;
  line-height: ${theme.input.lineHeight};
  padding: ${theme.input.padding};
`;

export const icon = css`
  fill: ${theme.colors.blue};
  height: 1em;
  margin-right: 0.5em;
  width: 1em;
`;

export const checkIn = css`
  align-items: center;
  display: flex;
  flex: 1;
  font-weight: 700;
  margin-right: 1em;
  user-select: none;
`;

export const checkOut = css`
  border-left: 1px solid ${theme.colors.lighterSeparator};
  flex: 1;
  font-weight: 700;
  padding-right: 0.8em;
  text-align: right;
  user-select: none;
`;

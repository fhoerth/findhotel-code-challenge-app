import React from 'react';

import agoraSvgUrl from '../../icons/svg/agoda.svg';
import bookingComSvgUrl from '../../icons/svg/booking.com.svg';
import expediaSvgUrl from '../../icons/svg/expedia.svg';
import hotelsComSvgUrl from '../../icons/svg/hotels.com.svg';

import { wrapper, icons } from './css';

const Footer: React.FC = () => (
  <footer css={wrapper}>
    <div css={icons}>
      <img src={expediaSvgUrl} alt="Expedia" />
      <img src={bookingComSvgUrl} alt="Booking.com" />
      <img src={hotelsComSvgUrl} alt="Hotels.com" />
      <img src={agoraSvgUrl} alt="Agoda" />
    </div>
  </footer>
);

export default Footer;

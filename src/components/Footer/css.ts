import { css } from '@emotion/react';

import theme from '../../theme';

export const wrapper = css`
  margin-top: 1em;
  width: 100%;
`;

export const icons = css`
  align-content: center;
  align-items: center;
  display: flex;
  flex: 1;
  justify-content: space-between;
  padding: ${theme.searchBoxPadding};
  user-select: none;

  img {
    max-height: 1.2em;
    max-width: 24%;
  }
`;

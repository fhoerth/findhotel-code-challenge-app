import { css } from '@emotion/react';

import logoUrl from '../../icons/svg/logo.svg';

const url = `url("${logoUrl}");`;

const header = css`
  background-image: ${url};
  background-repeat: no-repeat;
  background-size: 6em;
  height: 1.5em;
  width: 100%;
`;

export default header;

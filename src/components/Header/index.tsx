import React from 'react';

import header from './css';

const Header: React.FC = () => <header css={header} />;

export default Header;

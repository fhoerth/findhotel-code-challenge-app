import React from 'react';

import Button from '../../Button';

import { SearchIcon } from '../../../icons';

import { icon, text, centerdot } from './css';

type ModalSearchButtonProps = {
  rooms: number;
  guests: number;
  onSearch: () => void;
  disabled?: boolean;
};

const ModalSearchButton: React.FC<ModalSearchButtonProps> = ({
  rooms,
  guests,
  onSearch,
  disabled,
}: ModalSearchButtonProps) => (
  <Button data-testid="modal-search-button" disabled={disabled} onClick={onSearch}>
    <SearchIcon css={icon} />
    <span css={text}> {rooms} rooms</span>
    <span css={centerdot} />
    <span css={text}> {guests} guests</span>
  </Button>
);

ModalSearchButton.defaultProps = {
  disabled: false,
};

export default ModalSearchButton;

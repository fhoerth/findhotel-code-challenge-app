import { css } from '@emotion/react';

import theme from '../../../theme';

export const icon = css`
  fill: ${theme.colors.lightBlue};
  height: 1em;
  margin-right: 2px;
  width: 1em;
`;

export const text = css`
  font-weight: 400;
  margin-left: 0.35em;
`;

export const centerdot = css`
  &:before {
    content: '·';
    display: inline-block;
    font-family: sans-serif;
    font-size: 1.5em;
    line-height: 0.5em;
    margin-left: 0.25em;
    position: relative;
  }

  content: '';
  line-height: 0;
`;

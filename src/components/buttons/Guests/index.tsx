import React from 'react';

import { PeopleIcon } from '../../../icons';

import Button from '../../Button';

import { button, icon } from './css';

type GuestsButtonProps = {
  guests: number;
  onClick: () => void;
  className?: string;
};

const GuestsButton: React.FC<GuestsButtonProps> = ({
  guests,
  onClick,
  className,
}: GuestsButtonProps) => (
  <Button data-testid="guests-button" outlined css={button} className={className} onClick={onClick}>
    <PeopleIcon css={icon} />
    {guests}
  </Button>
);

GuestsButton.defaultProps = {
  className: undefined,
};

export default GuestsButton;

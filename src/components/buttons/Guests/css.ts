import { css } from '@emotion/react';

import theme from '../../../theme';

export const button = css`
  background: ${theme.colors.white};
  border-color: ${theme.colors.darkerOutline};
  color: ${theme.colors.black};
  justify-content: flex-start;
  min-width: 3.5em;
  position: relative;
`;

export const icon = css`
  fill: ${theme.colors.blue};
  height: 1em;
  margin-right: 0.35em;
  width: 1em;
`;

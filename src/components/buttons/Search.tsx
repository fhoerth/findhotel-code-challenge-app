import React from 'react';

import Button from '../Button';

type SearchProps = {
  onSearch: () => void;
};

const Search: React.FC<SearchProps> = ({ onSearch }: SearchProps) => (
  <Button onClick={onSearch}>Search</Button>
);

export default Search;

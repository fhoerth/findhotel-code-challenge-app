import { css } from '@emotion/react';

import theme from '../../../theme';

export const icon = css`
  fill: ${theme.colors.blue};
  height: 1em;
  width: 1em;
`;

export const addRoom = css`
  margin-left: 0.35em;
`;

export default addRoom;

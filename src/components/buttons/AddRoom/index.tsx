import React from 'react';

import { PlusIcon } from '../../../icons';

import Button from '../../Button';

import { icon, addRoom } from './css';

type AddRoomProps = {
  onRoomAdd: () => void;
  disabled?: boolean;
  className?: string;
};

const AddRoom: React.FC<AddRoomProps> = ({ onRoomAdd, className, disabled }: AddRoomProps) => (
  <Button outlined disabled={disabled} className={className} onClick={onRoomAdd}>
    <PlusIcon css={icon} />
    <span css={addRoom}>Add Room</span>
  </Button>
);

AddRoom.defaultProps = {
  className: undefined,
  disabled: false,
};

export default AddRoom;

import styled from '@emotion/styled';

type ButtonProps = {
  outlined?: boolean;
  disabled?: boolean;
};

const Button = styled.button<ButtonProps>`
  align-items: center;
  background-color: ${({ outlined, theme }) =>
    outlined ? theme.colors.lightGray : theme.colors.blue};
  border: 1px;
  border-color: ${({ outlined, theme }) =>
    outlined ? theme.colors.lighterOutline : theme.colors.blue};
  border-radius: ${({ theme }) => theme.borderRadius};
  border-style: solid;
  color: ${({ outlined, theme }) => (outlined ? theme.colors.blue : theme.colors.lightBlue)};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
  display: flex;
  font-size: ${({ theme }) => theme.button.fontSize};
  font-weight: 700;
  justify-content: center;
  line-height: ${({ theme }) => theme.button.lineHeight};
  opacity: ${({ disabled }) => (disabled ? '.5' : '1')};
  outline: 0;
  padding: ${({ theme }) => theme.button.padding};
  width: 100%;
`;

export default Button;

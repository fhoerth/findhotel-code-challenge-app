import { css } from '@emotion/react';

import theme from '../../theme';

export const wrapper = css`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const title = css`
  color: ${theme.colors.darkGray};
  font-size: 1.5em;
  font-weight: 400;
  margin: 2em 0 3.5em;
  max-width: 9em;
  padding: 0;
`;

export const searchBox = css`
  background: ${theme.colors.white};
  border: 1px solid ${theme.colors.lighterOutline};
  border-radius: 5px;
  box-shadow: 0px 0px 8px -2px ${theme.colors.darkerOutline};
  font-size: 0.9em;
  padding: ${theme.searchBoxPadding};
`;

export const row = css`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 0.8em 0;
`;

export const reservationCalendar = css`
  flex: 1;
  margin-right: 0.5em;
`;

export const guestsButton = css`
  flex: 0;
  width: 3em;
`;

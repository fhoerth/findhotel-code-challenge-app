import React, { useState } from 'react';
import { useNavigate } from 'react-router';

import useDispatch from '~store/hooks/useDispatch';
import { actionCreators } from '~store';
import { QueryStringSerializer } from '~features/queryString';
import { roomsSerializer } from '~features/rooms';
import { useRoomsState, useGuestsSelector } from '~features/rooms/selectors';

import Content from '../Content';
import Header from '../Header';
import Footer from '../Footer';
import LocationInput from '../inputs/Location';
import ReservationCalendar from '../inputs/ReservationCalendar';
import GuestsButton from '../buttons/Guests';
import SearchButton from '../buttons/Search';

import GuestsAndRoomsModal from '../GuestsAndRoomsModal';

import { wrapper, title, searchBox, row, reservationCalendar, guestsButton } from './css';

const HomeScreen: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const state = useRoomsState({ from: 'nextState' });
  const guests = useGuestsSelector({ from: 'currentState' });

  const handleOpen = (): void => setVisible(true);

  const handleClose = (): void => {
    dispatch(actionCreators.resetRooms());
    setVisible(false);
  };

  const handleSearch = (): void => {
    const serializedState = roomsSerializer(state);
    const queryString = QueryStringSerializer.serializeRooms(serializedState);

    setVisible(false);
    navigate('/'.concat(queryString), { replace: true });
  };

  return (
    <>
      <GuestsAndRoomsModal visible={visible} onClose={handleClose} onSearch={handleSearch} />
      <Content>
        <Header />
        <main css={wrapper}>
          <h1 css={title}>Find the perfect deal, always.</h1>
          <div css={searchBox}>
            <LocationInput />
            <div css={row}>
              <ReservationCalendar css={reservationCalendar} />
              <GuestsButton css={guestsButton} guests={guests} onClick={handleOpen} />
            </div>
            <SearchButton onSearch={() => {}} />
          </div>
        </main>
        <Footer />
      </Content>
    </>
  );
};

export default HomeScreen;

import React from 'react';

import { background, content } from './css';

type ContentProps = {
  children: React.ReactNode;
};

const Content: React.FC<ContentProps> = ({ children }: ContentProps) => (
  <div css={background}>
    <div css={content}>{children}</div>
  </div>
);

export default Content;

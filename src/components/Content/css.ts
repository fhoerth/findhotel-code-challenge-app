import { css } from '@emotion/react';

import theme from '../../theme';

import url from './background.svg';

export const content = css`
  align-items: center;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  margin: 0 auto;
  max-width: ${theme.content.maxWidth};
  padding: 8px;
`;

export const background = css`
  background-image: url(${url});
  background-position: center bottom;
  background-size: cover;
  min-height: calc(100vh);
`;

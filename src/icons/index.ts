import CalendarIcon from './Calendar';
import CloseIcon from './Close';
import LocationIcon from './Location';
import MinusIcon from './Minus';
import NearMeIcon from './NearMe';
import PeopleIcon from './People';
import PlusIcon from './Plus';
import SearchIcon from './Search';

export { CalendarIcon };
export { CloseIcon };
export { LocationIcon };
export { MinusIcon };
export { NearMeIcon };
export { PeopleIcon };
export { PlusIcon };
export { SearchIcon };

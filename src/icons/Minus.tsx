import React from 'react';

type MinusIconProps = { className?: string };

const MinusIcon: React.FC<MinusIconProps> = ({ className }: MinusIconProps) => {
  const props = className ? { className } : {};

  return (
    <svg
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      height="20"
      viewBox="7 6 22 22"
      width="20">
      <path css={{ transform: 'scale(1.5)' }} d="M19 13H5v-2h14v2z" />
    </svg>
  );
};

MinusIcon.defaultProps = {
  className: undefined,
};

export default MinusIcon;

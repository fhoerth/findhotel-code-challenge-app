import React from 'react';

type PlusIconProps = { className?: string };

const PlusIcon: React.FC<PlusIconProps> = ({ className }: PlusIconProps) => {
  const props = className ? { className } : {};

  return (
    <svg
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      height="20"
      viewBox="5 5 20 20"
      width="20">
      <path css={{ transform: 'scale(1.25)' }} d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" />
    </svg>
  );
};

PlusIcon.defaultProps = {
  className: undefined,
};

export default PlusIcon;

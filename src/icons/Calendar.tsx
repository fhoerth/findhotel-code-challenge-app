import React from 'react';

type CalendarIconProps = { className?: string };

const CalendarIcon: React.FC<CalendarIconProps> = ({ className }: CalendarIconProps) => {
  const props = className ? { className } : {};

  return (
    <svg
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      height="24"
      viewBox="0 0 24 24"
      width="24">
      <path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z" />
      <circle cx="12" cy="9" r="2.5" />
    </svg>
  );
};

CalendarIcon.defaultProps = {
  className: undefined,
};

export default CalendarIcon;

import Rooms from './Rooms';

export const hydrate = (
  payload: Rooms.HydrateRoomsAction['payload'],
): Rooms.HydrateRoomsAction => ({
  type: Rooms.ActionType.HYDRATE,
  payload,
});

export const addRoom = (payload: Rooms.AddRoomAction['payload']): Rooms.AddRoomAction => ({
  type: Rooms.ActionType.ADD_ROOM,
  payload,
});

export const addAdultGuest = (
  payload: Rooms.AddAdultGuestAction['payload'],
): Rooms.AddAdultGuestAction => ({
  type: Rooms.ActionType.ADD_ADULT_GUEST,
  payload,
});

export const addChildGuest = (
  payload: Rooms.AddChildGuestAction['payload'],
): Rooms.AddChildGuestAction => ({
  type: Rooms.ActionType.ADD_CHILD_GUEST,
  payload,
});

export const updateChildGuest = (
  payload: Rooms.UpdateChildGuestAction['payload'],
): Rooms.UpdateChildGuestAction => ({
  type: Rooms.ActionType.UPDATE_CHILD_GUEST,
  payload,
});

export const removeRoom = (payload: Rooms.RemoveRoomAction['payload']): Rooms.RemoveRoomAction => ({
  type: Rooms.ActionType.REMOVE_ROOM,
  payload,
});

export const removeAdultGuest = (
  payload: Rooms.RemoveAdultGuestAction['payload'],
): Rooms.RemoveAdultGuestAction => ({
  type: Rooms.ActionType.REMOVE_ADULT_GUEST,
  payload,
});

export const removeChildGuest = (
  payload: Rooms.RemoveChildGuestAction['payload'],
): Rooms.RemoveChildGuestAction => ({
  type: Rooms.ActionType.REMOVE_CHILD_GUEST,
  payload,
});

export const removeLastChildGuest = (
  payload: Rooms.RemoveLastChildGuestAction['payload'],
): Rooms.RemoveLastChildGuestAction => ({
  type: Rooms.ActionType.REMOVE_LAST_CHILD_GUEST,
  payload,
});

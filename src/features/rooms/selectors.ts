import useSelector from '~store/hooks/useSelector';
import Rooms from './Rooms';
import { getRoom } from './utils';

type From = { from: 'currentState' | 'nextState' };
type RoomId = { id: Rooms.RoomId };

const useCurrentState = () => useSelector((state) => state.rooms.currentState);
const useNextState = () => useSelector((state) => state.rooms.nextState);
export const useRoomsState = ({ from }: From): Rooms.State => {
  let state: Rooms.State;

  if (from === 'currentState') {
    state = useCurrentState();
  } else {
    state = useNextState();
  }

  return state;
};

export const useRoomsSelector = (from: From): Array<Rooms.Room> => {
  const state = useRoomsState(from);

  return Object.values(state);
};

export const useGuestsSelector = (from: From) => {
  const rooms = useRoomsSelector(from);

  return rooms.reduce(
    (guests: number, room: Rooms.Room): number =>
      guests + room.guests.adults + room.guests.children.length,
    0,
  );
};

export const useRoomSelector = ({ from, id }: From & RoomId) => {
  const state = useRoomsState({ from });

  return getRoom(state, id);
};

import config from '../../config';

import Rooms from './Rooms';

const { locationParser } = config;
const { delimiters } = locationParser;

function childGuestSerializer(child: Rooms.Child & Required<Pick<Rooms.Child, 'age'>>): string {
  return child.age.toString();
}

function childGuestsSerializer(children: Array<Rooms.Child>): string | null {
  if (!children.length) {
    return null;
  }

  const agedChildren = children.filter((item) => typeof item.age !== 'undefined') as Array<
    Rooms.Child & Required<Pick<Rooms.Child, 'age'>>
  >;

  return agedChildren.map(childGuestSerializer).join(delimiters.child);
}

function adultGuestsSerializer(adults: number): string {
  return adults.toString();
}

function roomSerializer({ guests }: Rooms.Room): string {
  const { adults, children } = guests;

  return [adultGuestsSerializer(adults), childGuestsSerializer(children)]
    .filter(Boolean)
    .join(delimiters.guests);
}

function stateSerializer(state: Rooms.State): string {
  return Object.values(state).map(roomSerializer).join(delimiters.room);
}

export default stateSerializer;

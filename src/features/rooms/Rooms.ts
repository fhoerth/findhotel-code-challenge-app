module Rooms {
  export type Child = {
    key: number;
    age?: number;
  };

  export type RoomId = number;
  export type ChildId = number;
  export type Guests = {
    adults: number;
    children: Array<Child>;
  };
  export type Room = {
    id: RoomId;
    guests: Guests;
  };

  /**
   * State
   */
  export type State = Record<RoomId, Room>;

  /**
   * Actions
   */
  type BasePayload = { id: RoomId };

  export enum ActionType {
    HYDRATE = '@@rooms/HYDRATE',
    ADD_ROOM = '@@rooms/ADD_ROOM',
    ADD_ADULT_GUEST = '@@rooms/ADD_ADULT_GUEST',
    ADD_CHILD_GUEST = '@@rooms/ADD_CHILD_GUEST',
    UPDATE_CHILD_GUEST = '@@rooms/UPDATE_CHILD_GUEST',
    REMOVE_ROOM = '@@rooms/REMOVE_ROOM',
    REMOVE_ADULT_GUEST = '@@rooms/REMOVE_ADULT_GUEST',
    REMOVE_CHILD_GUEST = '@@rooms/REMOVE_CHILD_GUEST',
    REMOVE_LAST_CHILD_GUEST = '@@rooms/REMOVE_LAST_CHILD_GUEST',
  }

  export type HydrateRoomsAction = {
    type: ActionType.HYDRATE;
    payload: Rooms.State;
  };

  export type AddRoomAction = {
    type: ActionType.ADD_ROOM;
    payload: BasePayload;
  };

  export type AddAdultGuestAction = {
    type: ActionType.ADD_ADULT_GUEST;
    payload: BasePayload;
  };

  export type AddChildGuestAction = {
    type: ActionType.ADD_CHILD_GUEST;
    payload: BasePayload & { child?: Child };
  };

  export type UpdateChildGuestAction = {
    type: ActionType.UPDATE_CHILD_GUEST;
    payload: BasePayload & { child: Child; data: Omit<Child, 'key'> };
  };

  export type RemoveRoomAction = {
    type: ActionType.REMOVE_ROOM;
    payload: BasePayload;
  };

  export type RemoveAdultGuestAction = {
    type: ActionType.REMOVE_ADULT_GUEST;
    payload: BasePayload;
  };

  export type RemoveChildGuestAction = {
    type: ActionType.REMOVE_CHILD_GUEST;
    payload: BasePayload & { child: Child };
  };

  export type RemoveLastChildGuestAction = {
    type: ActionType.REMOVE_LAST_CHILD_GUEST;
    payload: BasePayload;
  };

  export type Action =
    | HydrateRoomsAction
    | AddRoomAction
    | AddAdultGuestAction
    | AddChildGuestAction
    | UpdateChildGuestAction
    | RemoveRoomAction
    | RemoveAdultGuestAction
    | RemoveChildGuestAction
    | RemoveLastChildGuestAction;
}

export default Rooms;

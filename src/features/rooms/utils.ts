import Rooms from './Rooms';

export function getRoom(state: Rooms.State, id: Rooms.RoomId): Rooms.Room {
  const room = state[id];

  if (!room) {
    throw new Error(`Room number "${id}" not found.`);
  }

  return room;
}

function getHighest(numbers: Array<number | string>): number {
  const highestId = numbers.reduce(
    (result: number, key: string | number): number =>
      parseInt(key.toString(), 10) > result ? parseInt(key.toString(), 10) : result,
    0,
  );

  return highestId;
}

export function createRoomId(state: Rooms.State): Rooms.RoomId {
  const keys = Object.keys(state);

  return getHighest(keys) + 1;
}

export function createChildKey(children: Array<Rooms.Child>): Rooms.ChildId {
  const ids = children.map((child) => child.key);

  return getHighest(ids) + 1;
}

export function updateChildGuest(
  room: Rooms.Room,
  child: Rooms.Child,
  data: Omit<Rooms.Child, 'key'>,
): Rooms.Room {
  const { children } = room.guests;

  const foundChildIndex = children.findIndex((item) => item === child);

  if (foundChildIndex === -1) {
    throw new Error('Child not found.');
  }

  const foundChild = children[foundChildIndex];
  const newChild = {
    ...foundChild,
    ...data,
  };

  const newChildren = [...children];
  newChildren[foundChildIndex] = newChild;

  return {
    ...room,
    guests: {
      ...room.guests,
      children: newChildren,
    },
  };
}

export function removeRoom(state: Rooms.State, id: Rooms.RoomId): Rooms.State {
  const nextState = { ...state };

  delete nextState[id];

  return nextState;
}

export function removeAdultGuest(room: Rooms.Room): Rooms.Room {
  return {
    ...room,
    guests: {
      ...room.guests,
      adults: room.guests.adults - 1,
    },
  };
}

export function removeChildGuest(room: Rooms.Room, child: Rooms.Child): Rooms.Room {
  const { guests } = room;
  const children = guests.children.filter((item) => item !== child);

  return {
    ...room,
    guests: {
      ...guests,
      children,
    },
  };
}

export function removeLastChildGuest(room: Rooms.Room): Rooms.Room {
  const { guests } = room;
  const children = guests.children.slice(0, guests.children.length - 1);

  return {
    ...room,
    guests: {
      ...guests,
      children,
    },
  };
}

export function chainReducer<T extends Rooms.State>(
  previousState: T,
  ...args: Array<(nextState: T) => T>
): T {
  const [head, ...tail] = args;

  const nextState = head(previousState);

  if (!tail.length) {
    return nextState;
  }

  return chainReducer(nextState, ...tail);
}

import stateParser from '../stateParser';

import { room, cases, entry, result } from './test-utils';

describe('stateParser', () => {
  test.each(cases)(`parses %s`, (stringifiedRooms, expectedRoom) => {
    expect(stateParser(stringifiedRooms)).toEqual(expectedRoom);
  });

  test('parses all cases', () => {
    expect(stateParser(entry)).toEqual(result);
  });

  test('fails when entry has invalid has invalid adult guests', () => {
    expect(() => stateParser('1|:3')).toThrow();
  });

  test(`doesn't fail when entry has invalid child guests`, () => {
    expect(stateParser('3:|3:|3')).toEqual({
      ...room(1, { adults: 3, children: [] }),
      ...room(2, { adults: 3, children: [] }),
      ...room(3, { adults: 3, children: [] }),
    });
  });
});

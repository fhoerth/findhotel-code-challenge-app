import Rooms from '../Rooms';
import reducer from '../reducer';
import { chainReducer } from '../utils';
import * as actionCreators from '../actionCreators';

import { room } from './test-utils';

const emptyState = {};

describe('roomsReducer', () => {
  test('sets initial state', () => {
    const anyAction = ({} as unknown) as Rooms.Action;
    const initialState = reducer(undefined, anyAction);
    expect(initialState).toEqual({
      1: {
        id: 1,
        guests: {
          adults: 2,
          children: [],
        },
      },
    });
  });

  test('hydrates state', () => {
    const state = {
      ...room(1, { adults: 5, children: [] }),
    };
    const initialState = reducer(emptyState, actionCreators.hydrate(state));

    expect(initialState).toEqual(state);
  });

  test('adds a room', () => {
    const result = reducer(emptyState, actionCreators.addRoom({ id: 1 }));

    expect(result).toEqual({
      ...room(1, {
        adults: 0,
        children: [],
      }),
    });
  });

  test('adds adult guests', () => {
    const result1 = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 2 })),
    );

    const expectedResult1 = {
      ...room(1, {
        adults: 0,
        children: [],
      }),
      ...room(2, {
        adults: 0,
        children: [],
      }),
    };

    expect(result1).toEqual(expectedResult1);

    const result2 = chainReducer(
      result1,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 3 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 3 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 3 })),
    );

    expect(result2).toEqual({
      ...expectedResult1,
      ...room(3, {
        adults: 2,
        children: [],
      }),
    });
  });

  test('adds child guests', () => {
    const child1 = { key: 1, age: 1 };
    const child3 = { key: 1, age: 2 };
    const child4 = { key: 2, age: 3 };

    const result1 = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1 })),
    );

    const expectedResult1 = {
      ...room(1, {
        adults: 1,
        children: [child1, { key: 2 }],
      }),
    };

    expect(result1).toEqual(expectedResult1);

    const result2 = chainReducer(
      result1,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 2 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 2 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 2, child: child3 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 2, child: child4 })),
    );

    expect(result2).toEqual({
      ...expectedResult1,
      ...room(2, {
        adults: 1,
        children: [child3, child4],
      }),
    });
  });

  test('updates child', () => {
    const child = { key: 1, age: 0 };
    const updatedChild = { age: 1 };

    const result = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child })),
      (nextState) =>
        reducer(nextState, actionCreators.updateChildGuest({ id: 1, child, data: updatedChild })),
    );

    expect(result).toEqual({
      ...room(1, {
        adults: 0,
        children: [{ ...child, ...updatedChild }],
      }),
    });
  });

  test('removes rooms', () => {
    const result = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 2 })),
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 3 })),
      (nextState) => reducer(nextState, actionCreators.removeRoom({ id: 2 })),
    );

    expect(result).toEqual({
      ...room(1, { adults: 0, children: [] }),
      ...room(3, { adults: 0, children: [] }),
    });
  });

  test('removes adult guests', () => {
    const result = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addAdultGuest({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.removeAdultGuest({ id: 1 })),
    );

    expect(result).toEqual(room(1, { adults: 2, children: [] }));
  });

  test('removes child guests', () => {
    const child1 = { key: 1, age: 0 };
    const child2 = { key: 2, age: 0 };
    const child3 = { key: 3, age: 0 };
    const result = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child2 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child3 })),
      (nextState) => reducer(nextState, actionCreators.removeChildGuest({ id: 1, child: child2 })),
    );

    const expectedChildren = [child1, child3];
    expect(result).toEqual(room(1, { adults: 0, children: expectedChildren }));
  });

  test('removes last child guest', () => {
    const child1 = { key: 1, age: 0 };
    const child2 = { key: 2, age: 0 };
    const result = chainReducer(
      emptyState,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id: 1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child1 })),
      (nextState) => reducer(nextState, actionCreators.addChildGuest({ id: 1, child: child2 })),
      (nextState) => reducer(nextState, actionCreators.removeLastChildGuest({ id: 1 })),
    );

    const expectedChildren = [child1];

    expect(result).toEqual(room(1, { adults: 0, children: expectedChildren }));
  });

  test('throws error if an invalid id is provided', () => {
    const child = { key: 1, age: 6 };

    expect(() => reducer(emptyState, actionCreators.addAdultGuest({ id: 1 }))).toThrowError();
    expect(() =>
      reducer(emptyState, actionCreators.addChildGuest({ id: 1, child })),
    ).toThrowError();
  });
});

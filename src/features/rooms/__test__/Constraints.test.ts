import Constraints from '../Constraints';
import Rooms from '../Rooms';

import { room } from './test-utils';

const child = { key: 1, age: 0 };
describe('Constraints', () => {
  test('limits rooms length', () => {
    const initialState = {
      ...room(1, { adults: 2, children: [] }),
    };
    expect(Constraints.canRoomBeAdded(initialState)).toBe(true);
    expect(Constraints.canRoomBeRemoved(initialState)).toBe(false);

    const entry = [1, 2, 3, 4, 5, 6, 7, 8].reduce(
      (state: Rooms.State, id): Rooms.State => ({
        ...state,
        ...room(id, { adults: 0, children: [] }),
      }),
      {},
    );

    expect(Constraints.canRoomBeAdded(entry)).toBe(false);
  });

  test('limits adult guests occupancy', () => {
    const entry1 = { id: 1, guests: { adults: 1, children: [] } };
    const entry2 = { id: 1, guests: { adults: 4, children: [] } };
    const entry3 = { id: 1, guests: { adults: 5, children: [] } };
    const entry4 = { id: 1, guests: { adults: 2, children: [child, child, child] } };

    expect(Constraints.canAdultGuestBeAdded(entry1)).toBe(true);
    expect(Constraints.canAdultGuestBeRemoved(entry1)).toBe(false);

    expect(Constraints.canAdultGuestBeAdded(entry2)).toBe(true);
    expect(Constraints.canAdultGuestBeRemoved(entry2)).toBe(true);

    expect(Constraints.canAdultGuestBeAdded(entry3)).toBe(false);
    expect(Constraints.canAdultGuestBeRemoved(entry3)).toBe(true);

    expect(Constraints.canAdultGuestBeAdded(entry4)).toBe(false);
    expect(Constraints.canAdultGuestBeRemoved(entry4)).toBe(true);
  });

  test('limits child guests occupancy', () => {
    const entry1 = { id: 1, guests: { adults: 2, children: [child, child] } };
    const entry2 = { id: 1, guests: { adults: 3, children: [child, child] } };
    const entry3 = { id: 1, guests: { adults: 4, children: [child] } };
    const entry4 = { id: 1, guests: { adults: 5, children: [] } };

    expect(Constraints.canAdultGuestBeAdded(entry1)).toBe(true);
    expect(Constraints.canAdultGuestBeAdded(entry2)).toBe(false);
    expect(Constraints.canAdultGuestBeAdded(entry3)).toBe(false);
    expect(Constraints.canAdultGuestBeAdded(entry4)).toBe(false);
  });

  test('constraints search', () => {
    const emptyChild = { key: 1 };
    const entry1 = {
      ...room(1, {
        adults: 1,
        children: [],
      }),
      ...room(2, {
        adults: 2,
        children: [child],
      }),
    };
    const entry2 = {
      ...entry1,
      ...room(3, {
        adults: 3,
        children: [child, emptyChild],
      }),
    };
    const entry3 = {
      ...entry1,
      ...room(3, {
        adults: 0,
        children: [child, child],
      }),
    };

    expect(Constraints.canSearchBePerformed(entry1)).toBe(true);
    expect(Constraints.canSearchBePerformed(entry2)).toBe(false);
    expect(Constraints.canSearchBePerformed(entry3)).toBe(false);
  });
});

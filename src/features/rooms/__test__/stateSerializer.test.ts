import stateSerializer from '../stateSerializer';

import { room, cases, entry, result } from './test-utils';

describe('stateSerializer', () => {
  test.each(cases)(`serializes %s`, (expectedResult, rooms) => {
    expect(stateSerializer(rooms)).toEqual(expectedResult);
  });

  test('serializes only aged children', () => {
    const rooms = {
      ...room(1, {
        adults: 1,
        children: [{ key: 1 }, { key: 2, age: 0 }],
      }),
    };

    expect(stateSerializer(rooms)).toEqual('1:0');
  });

  test('serializes all cases', () => {
    expect(stateSerializer(result)).toEqual(entry);
  });
});

import Rooms from '../Rooms';
import { createRoomId } from '../utils';

export const room = (id: number, guests: Rooms.Guests): Rooms.State => ({
  [id]: {
    id,
    guests,
  },
});

export const cases: Array<[string, Rooms.State]> = [
  [
    '1:4,6|3',
    {
      ...room(1, {
        adults: 1,
        children: [
          { key: 1, age: 4 },
          { key: 2, age: 6 },
        ],
      }),
      ...room(2, {
        adults: 3,
        children: [],
      }),
    },
  ],
  [
    '3',
    {
      ...room(1, {
        adults: 3,
        children: [],
      }),
    },
  ],
  [
    '2:4',
    {
      ...room(1, {
        adults: 2,
        children: [{ key: 1, age: 4 }],
      }),
    },
  ],
  [
    '1:0,13,16',
    {
      ...room(1, {
        adults: 1,
        children: [
          { key: 1, age: 0 },
          { key: 2, age: 13 },
          { key: 3, age: 16 },
        ],
      }),
    },
  ],
];

export const entry = cases.map(([caseEntry]) => caseEntry).join('|');
export const result = cases
  .map(([, caseResult]) => Object.values(caseResult))
  .reduce((rooms, item) => rooms.concat(item))
  .reduce(
    (state: Rooms.State, item): Rooms.State => ({
      [createRoomId(state)]: {
        id: createRoomId(state),
        guests: item.guests,
      },
      ...state,
    }),
    {},
  );

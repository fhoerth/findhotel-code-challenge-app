import config from '../../config';

import Rooms from './Rooms';
import reducer from './reducer';
import { chainReducer, getRoom, createRoomId, createChildKey } from './utils';
import * as actionCreators from './actionCreators';

const { locationParser } = config;
const { delimiters } = locationParser;

function childGuestParser(room: Rooms.Room, entry: string, state: Rooms.State): Rooms.State {
  const age = parseInt(entry, 10);

  if (Number.isNaN(age)) {
    throw new Error('Unable to parse child');
  }

  const { id, guests } = room;
  const key = createChildKey(guests.children);
  const child = { key, age };

  return reducer(state, actionCreators.addChildGuest({ id, child }));
}

function childGuestsParser(room: Rooms.Room, entry: string, state: Rooms.State): Rooms.State {
  if (!entry) {
    return state;
  }

  const result = entry
    .split(delimiters.child)
    .map((childGuestParserEntry) => (nextState: Rooms.State) =>
      childGuestParser(getRoom(nextState, room.id), childGuestParserEntry, nextState),
    );

  return chainReducer(state, ...result);
}

function adultGuestsParser(room: Rooms.Room, entry: string, state: Rooms.State): Rooms.State {
  const adults = parseInt(entry, 10);

  if (Number.isNaN(adults)) {
    throw new Error('Unable to parse adults');
  }

  if (adults < 1) {
    throw new Error('Invalid entry');
  }

  const range = Array.from(Array(adults).keys());
  const { id } = room;

  return chainReducer(
    state,
    ...range.map(() => (nextState: Rooms.State) =>
      reducer(nextState, actionCreators.addAdultGuest({ id })),
    ),
  );
}

function roomParser(entry: string): (nextState: Rooms.State) => Rooms.State {
  const [stringifiedAdults, stringifiedChildren] = entry.split(delimiters.guests);

  return (state) => {
    const id = createRoomId(state);

    return chainReducer(
      state,
      (nextState) => reducer(nextState, actionCreators.addRoom({ id })),
      (nextState) => adultGuestsParser(getRoom(nextState, id), stringifiedAdults, nextState),
      (nextState) => childGuestsParser(getRoom(nextState, id), stringifiedChildren, nextState),
    );
  };
}

function stateParser(serializedState: string): Rooms.State {
  const initialState = {};
  const stringifiedRooms = serializedState.split(delimiters.room);

  return chainReducer(
    initialState,
    ...stringifiedRooms.map((roomParserEntry) => roomParser(roomParserEntry)),
  );
}

export default stateParser;

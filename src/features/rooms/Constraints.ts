import Rooms from './Rooms';

import config from '../../config';

const { rooms } = config;
const { constraints } = rooms;

class Constraints {
  private static canGuestBeAdded({ guests }: Rooms.Room): boolean {
    const { adults, children } = guests;

    return adults + children.length < constraints.maxGuests;
  }

  private static getLength(state: Rooms.State): number {
    const { length } = Object.keys(state);

    return length;
  }

  static canRoomBeRemoved(state: Rooms.State): boolean {
    return Constraints.getLength(state) > 1;
  }

  static canRoomBeAdded(state: Rooms.State): boolean {
    return Constraints.getLength(state) < constraints.maxRooms;
  }

  static canAdultGuestBeRemoved({ guests }: Rooms.Room): boolean {
    const { adults } = guests;

    return adults > constraints.minAdultGuests;
  }

  static canAdultGuestBeAdded(room: Rooms.Room): boolean {
    return Constraints.canGuestBeAdded(room);
  }

  static canChildGuestBeAdded(room: Rooms.Room): boolean {
    const { guests } = room;
    const { children } = guests;

    return this.canGuestBeAdded(room) && children.length < constraints.maxChildGuests;
  }

  private static isRoomValid({ guests }: Rooms.Room): boolean {
    const { adults, children } = guests;

    return !children.find((child) => typeof child.age === 'undefined') && !!adults;
  }

  static canSearchBePerformed(state: Rooms.State): boolean {
    return Object.values(state).reduce(
      (previousValue, currentValue) => previousValue && Constraints.isRoomValid(currentValue),
      true as boolean,
    );
  }
}

export default Constraints;

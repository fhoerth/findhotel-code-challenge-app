import Rooms from './Rooms';

import Constraints from './Constraints';
import { addAdultGuest, addRoom } from './actionCreators';
import * as utils from './utils';

function reducer(state: Rooms.State, action: Rooms.Action): Rooms.State {
  switch (action.type) {
    case Rooms.ActionType.HYDRATE: {
      const { payload } = action;

      return payload;
    }
    case Rooms.ActionType.ADD_ROOM: {
      if (Constraints.canRoomBeAdded(state)) {
        const { payload } = action;
        const { id } = payload;

        const room: Rooms.Room = {
          id,
          guests: {
            adults: 0,
            children: [],
          },
        };

        return {
          ...state,
          [id]: room,
        };
      }

      return state;
    }
    case Rooms.ActionType.ADD_ADULT_GUEST: {
      const { payload } = action;
      const { id } = payload;
      const room = utils.getRoom(state, id);

      if (Constraints.canAdultGuestBeAdded(room)) {
        const { guests } = room;

        const nextRoom: Rooms.Room = {
          id,
          guests: {
            ...guests,
            adults: guests.adults + 1,
          },
        };

        return {
          ...state,
          [id]: nextRoom,
        };
      }

      return state;
    }
    case Rooms.ActionType.ADD_CHILD_GUEST: {
      const { payload } = action;
      const { id } = payload;
      const room = utils.getRoom(state, id);

      if (Constraints.canChildGuestBeAdded(room)) {
        const { child } = payload;
        const { guests } = room;

        const emptyChild = {
          key: utils.createChildKey(guests.children),
        };
        const nextRoom: Rooms.Room = {
          ...room,
          guests: {
            ...guests,
            children: guests.children.concat(child || emptyChild),
          },
        };

        return {
          ...state,
          [id]: nextRoom,
        };
      }

      return state;
    }
    case Rooms.ActionType.UPDATE_CHILD_GUEST: {
      const { payload } = action;
      const { id, child, data } = payload;
      const room = utils.getRoom(state, id);
      const nextRoom: Rooms.Room = utils.updateChildGuest(room, child, data);

      return {
        ...state,
        [id]: nextRoom,
      };
    }
    case Rooms.ActionType.REMOVE_ROOM: {
      if (Constraints.canRoomBeRemoved(state)) {
        const { payload } = action;
        const { id } = payload;

        return utils.removeRoom(state, id);
      }

      return state;
    }
    case Rooms.ActionType.REMOVE_ADULT_GUEST: {
      const { payload } = action;
      const { id } = payload;

      const room = utils.getRoom(state, id);

      if (Constraints.canAdultGuestBeRemoved(room)) {
        return {
          ...state,
          [id]: utils.removeAdultGuest(room),
        };
      }

      return state;
    }
    case Rooms.ActionType.REMOVE_CHILD_GUEST: {
      const { payload } = action;
      const { id, child } = payload;

      const room = utils.getRoom(state, id);

      return {
        ...state,
        [id]: utils.removeChildGuest(room, child),
      };
    }
    case Rooms.ActionType.REMOVE_LAST_CHILD_GUEST: {
      const { payload } = action;
      const { id } = payload;

      const room = utils.getRoom(state, id);

      return {
        ...state,
        [id]: utils.removeLastChildGuest(room),
      };
    }
    default:
      return state;
  }
}

function defaultState() {
  /**
   * Adds 1 room with 2 adult guests.
   */
  const initialState = {};
  const id = utils.createRoomId(initialState);

  return utils.chainReducer(
    initialState,
    (nextState) => reducer(nextState, addRoom({ id })),
    (nextState) => reducer(nextState, addAdultGuest({ id })),
    (nextState) => reducer(nextState, addAdultGuest({ id })),
  );
}

function roomsReducer(state: Rooms.State = defaultState(), action: Rooms.Action): Rooms.State {
  return reducer(state, action);
}

export default roomsReducer;

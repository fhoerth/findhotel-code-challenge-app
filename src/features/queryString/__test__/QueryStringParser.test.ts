import { createMemoryHistory } from 'history';

import QueryStringParser from '../Parser';

describe('QueryStringParser', () => {
  test('parses rooms state from url', () => {
    const entry = '$ROOM_STATE';
    const encodedEntry = encodeURIComponent(entry);
    const history = createMemoryHistory({
      initialEntries: [`?rooms=${encodedEntry}`],
    });
    const result = QueryStringParser.parseRooms(history.location);

    expect(result).toBe(entry);
  });

  test('returns null', () => {
    const history = createMemoryHistory({
      initialEntries: ['?anotherQueryParam=$ROOM_STATE'],
    });
    const result = QueryStringParser.parseRooms(history.location);

    expect(result).toBe(null);
  });
});

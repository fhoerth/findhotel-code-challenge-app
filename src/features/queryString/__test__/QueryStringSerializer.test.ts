import UrlSerializer from '../Serializer';

describe('UrlSerializer', () => {
  test('serializes rooms state', () => {
    const entry = '$ROOM_STATE';
    const encodedEntry = encodeURIComponent(entry);
    const result = UrlSerializer.serializeRooms(entry);

    expect(result).toBe(`?rooms=${encodedEntry}`);
  });
});

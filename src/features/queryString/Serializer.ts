import queryString from 'query-string';

import config from '../../config';

class QueryStringSerializer {
  static serialize(key: string, value: unknown): string {
    const queryParms = {
      [key]: value,
    };

    return '?'.concat(queryString.stringify(queryParms));
  }

  static serializeRooms(serializedRoomState: string): string {
    return QueryStringSerializer.serialize(config.queryParams.rooms, serializedRoomState);
  }
}

export default QueryStringSerializer;

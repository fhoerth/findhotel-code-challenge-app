import { Location } from 'history';
import queryString from 'query-string';

import config from '../../config';

class QueryStringParser {
  static parse(location: Location, key: string): string | null {
    const parsedQuery = queryString.parse(location.search);
    const serializedRoomState = parsedQuery[key];

    if (typeof serializedRoomState !== 'string') {
      return null;
    }

    return decodeURIComponent(serializedRoomState);
  }

  static parseRooms(location: Location) {
    return QueryStringParser.parse(location, config.queryParams.rooms);
  }
}

export default QueryStringParser;

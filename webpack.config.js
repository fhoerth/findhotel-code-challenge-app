const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const { env, isDevelopment, isProduction } = require('./env');
const config = require('./config');

const staticRelativePath = config.paths.static.replace(/^\//, '');
const file = (filename) => `${staticRelativePath}/${filename}`;

const mode = isProduction ? 'production' : 'development';
const devtool = isProduction ? false : 'source-map';

const prodPlugins = [];
const devPlugins = [];
const plugins = [
  ...(isProduction ? prodPlugins : devPlugins),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(env),
  }),
  new HtmlWebpackPlugin(),
  new CleanWebpackPlugin(),
];

const rules = {
  tsx: {
    test: /\.(tsx?)$/,
    enforce: 'pre',
    exclude: /node_modules/,
    use: ['babel-loader', 'ts-loader'],
  },
  svg: {
    test: /\.svg$/,
    use: [
      {
        loader: 'svg-url-loader',
        options: {
          limit: 512,
          name: file('[name].[hash].svg'),
        },
      },
    ],
  },
  png: {
    test: /\.png$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          limit: 512,
          name: file('[name].[hash].png'),
        },
      },
    ],
  },
};

module.exports = {
  mode,
  devtool,
  plugins,
  devServer: {
    hot: true,
    writeToDisk: true,
    historyApiFallback: true,
    port: env.port,
    contentBase: config.directories.output,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    alias: {
      '~store': path.join(__dirname, 'src', 'store'),
      '~testing': path.join(__dirname, 'src', 'testing'),
      '~features': path.join(__dirname, 'src', 'features'),
    },
  },
  entry: {
    main: (isDevelopment
      ? [`webpack-dev-server/client?http://0.0.0.0:${env.port}`, 'webpack/hot/only-dev-server']
      : []
    ).concat([path.join(config.directories.input, 'entry.tsx')]),
  },
  output: {
    path: config.directories.output,
    publicPath: '/',
    filename: isProduction ? file('[name].[contenthash].js') : file('[name].js'),
  },
  module: {
    rules: [rules.tsx, rules.svg, rules.png],
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'all',
          name: 'vendor',
          enforce: true,
        },
      },
    },
  },
};
